let () =
    let deck = Deck.newDeck in
    let rec print lst = match lst with
        | tete::queue   -> print_endline tete; print queue
        | []            -> print_char '\n'
    in
    print_endline "Display deck content:";
    print (Deck.toStringList deck);
    print_endline "Display deck content verbosely:";
    print (Deck.toStringListVerbose deck);

    print_endline "Draw card in the deck until exception:";
    let rec draw_until_exception deck_rest = match (Deck.drawCard deck_rest) with
        | (a, b)    -> print_endline (Deck.Card.toStringVerbose a); draw_until_exception b
    in
    draw_until_exception deck
