module Card =
struct

    module Color =
    struct

        type t = Spade | Heart | Diamond | Club

        let all = Spade :: Heart :: Diamond :: Club :: []

        let toString card = match card with
            | Spade     -> "S"
            | Heart     -> "H"
            | Diamond   -> "D"
            | Club      -> "C"

        let toStringVerbose card = match card with
            | Spade     -> "Spade"
            | Heart     -> "Heart"
            | Diamond   -> "Diamond"
            | Club      -> "Club"

    end


    module Value =
    struct

        type t = T2 | T3 | T4 | T5 | T6 | T7 | T8 | T9 | T10 | Jack | Queen | King | As

        let all = T2 :: T3 :: T4 :: T5 :: T6 :: T7 :: T8 :: T9 :: T10 :: Jack :: Queen :: King :: As :: []

        let toInt card = match card with
            | T2        -> 1
            | T3        -> 2
            | T4        -> 3
            | T5        -> 4
            | T6        -> 5
            | T7        -> 6
            | T8        -> 7
            | T9        -> 8
            | T10       -> 9
            | Jack      -> 10
            | Queen     -> 11
            | King      -> 12
            | As        -> 13

        let toString card = match card with
            | T2        -> "2"
            | T3        -> "3"
            | T4        -> "4"
            | T5        -> "5"
            | T6        -> "6"
            | T7        -> "7"
            | T8        -> "8"
            | T9        -> "9"
            | T10       -> "10"
            | Jack      -> "J"
            | Queen     -> "Q"
            | King      -> "K"
            | As        -> "A"

        let toStringVerbose card = match card with
            | T2        -> "2"
            | T3        -> "3"
            | T4        -> "4"
            | T5        -> "5"
            | T6        -> "6"
            | T7        -> "7"
            | T8        -> "8"
            | T9        -> "9"
            | T10       -> "10"
            | Jack      -> "Jack"
            | Queen     -> "Queen"
            | King      -> "King"
            | As        -> "As"

        let next card = match card with
            | T2        -> T3
            | T3        -> T4
            | T4        -> T5
            | T5        -> T6
            | T6        -> T7
            | T7        -> T8
            | T8        -> T9
            | T9        -> T10
            | T10       -> Jack
            | Jack      -> Queen
            | Queen     -> King
            | King      -> As
            | As        -> invalid_arg "As is the last card"

        let previous card = match card with
            | T2        -> invalid_arg "2 is the first card"
            | T3        -> T2
            | T4        -> T3
            | T5        -> T4
            | T6        -> T5
            | T7        -> T6
            | T8        -> T7
            | T9        -> T8
            | T10       -> T9
            | Jack      -> T10
            | Queen     -> Jack
            | King      -> Queen
            | As        -> King

    end


    type t = {
        value : Value.t;
        color : Color.t;
    }

    let newCard value color = {
        value = value;
        color = color;
    }


    let allSpades =
        let rec loop lst = match lst with
            | tete::queue   -> ({color = Color.Spade; value = tete}) :: (loop queue)
            | []            -> []
        in
        loop Value.all

    let allHearts =
        let rec loop lst = match lst with
            | tete::queue -> ({color = Color.Heart; value = tete}) :: (loop queue)
            | []            -> []
        in
        loop Value.all

    let allDiamonds =
        let rec loop lst = match lst with
            | tete::queue -> ({color = Color.Diamond; value = tete}) :: (loop queue)
            | []            -> []
        in
        loop Value.all

    let allClubs =
        let rec loop lst = match lst with
            | tete::queue -> ({color = Color.Club; value = tete}) :: (loop queue)
            | []            -> []
        in
        loop Value.all

    let all = allSpades @ allHearts @ allDiamonds @ allClubs


    let getValue card = card.value

    let getColor card = card.color


    let toString card =
        (Value.toString card.value) ^ (Color.toString card.color)

    let toStringVerbose card = 
        Printf.sprintf "Card(%s, %s)" (Value.toStringVerbose card.value) (Color.toStringVerbose card.color)


    let compare card1 card2 =
        let v1 = Value.toInt card1.value in
        let v2 = Value.toInt card2.value in
        if v1 == v2 then 0
        else if v1 < v2 then -1
        else 1
     
    let max card1 card2 =
        let v1 = Value.toInt card1.value in
        let v2 = Value.toInt card2.value in
        if v1 < v2 then card2
        else card1

    let min card1 card2 =
        let v1 = Value.toInt card1.value in
        let v2 = Value.toInt card2.value in
        if v1 > v2 then card2
        else card1

    let best lst = match lst with
        | tete::queue   -> List.fold_left max tete queue
        | []            -> invalid_arg "List is empty"


    let isOf card color = (card.color == color)
    let isSpade card = (card.color == Color.Spade)
    let isHeart card = (card.color == Color.Heart)
    let isDiamond card = (card.color == Color.Diamond)
    let isClub card = (card.color == Color.Club)

end

type t = Card.t list

let _ = Random.self_init()

let newDeck =
    let shuffle d =
        let nd = List.map (fun c -> (Random.bits (), c)) d in
        let sond = List.sort compare nd in
        List.map snd sond
    in
    shuffle Card.all

let toStringList deck =
    List.map Card.toString deck

let toStringListVerbose deck =
    List.map Card.toStringVerbose deck

let drawCard deck = match deck with
    | tete::queue   -> (tete, queue)
    | []            -> invalid_arg "Empty deck"
