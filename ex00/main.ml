let card = Color.Club in
print_endline (Color.toStringVerbose card)

let rec display lst = match lst with
    | []            -> print_char '\n'
    | tete::queue   -> print_endline (Color.toString tete); display queue

let () = display Color.all
