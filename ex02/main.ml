let () =
    let rec loop1 lst = match lst with
        | tete::queue   -> print_endline (Card.toStringVerbose tete); loop1 queue
        | []            -> print_char '\n'
    in
    let rec loop2 lst = match lst with
        | tete::queue   -> print_endline (Card.toString tete); loop2 queue
        | []            -> print_char '\n'
    in
    loop1 Card.allSpades;
    loop1 Card.allHearts;
    loop1 Card.allDiamonds;
    loop1 Card.allClubs;

    loop2 Card.all;

    print_string "Compare As and As: ";
    if (compare Card.Value.As Card.Value.As) != 0 then print_endline "not equal" else print_endline "equal";

    print_string "Compare As and King: ";
    if (compare Card.Value.As Card.Value.King) != 0 then print_endline "not equal" else print_endline "equal";
    print_char '\n';

    print_string "Min As and King: ";
    if (Card.min (Card.newCard Card.Value.As Card.Color.Spade) (Card.newCard Card.Value.King Card.Color.Spade)) == (Card.newCard Card.Value.King Card.Color.Spade) then print_endline "King" else print_endline "As";

    print_string "Max As and King: ";
    if (Card.max (Card.newCard Card.Value.As Card.Color.Spade) (Card.newCard Card.Value.King Card.Color.Spade)) == (Card.newCard Card.Value.King Card.Color.Spade) then print_endline "King" else print_endline "As";
    print_char '\n';

    print_string "Best in a list of 7, king & jack - all of spade: ";
    let lst_comp = (Card.newCard Card.Value.T7 Card.Color.Spade) :: (Card.newCard Card.Value.King Card.Color.Spade) :: (Card.newCard Card.Value.Jack Card.Color.Spade) :: [] in
    print_endline (Card.toStringVerbose (Card.best lst_comp));
    print_char '\n';

    print_string "Check if king of spade isOf spade: ";
    if (Card.isOf (Card.newCard Card.Value.King Card.Color.Spade) Card.Color.Spade) then print_endline "true" else print_endline "false";

    print_string "Check if king of spade isOf heart: ";
    if (Card.isOf (Card.newCard Card.Value.King Card.Color.Spade) Card.Color.Heart) then print_endline "true" else print_endline "false";

    print_string "Check if king of spade isSpade: ";
    if (Card.isSpade (Card.newCard Card.Value.King Card.Color.Spade)) then print_endline "true" else print_endline "false";

    print_string "Check if king of spade isHeart: ";
    if (Card.isHeart (Card.newCard Card.Value.King Card.Color.Spade)) then print_endline "true" else print_endline "false"
