let rec display_front card =
    if card != Value.As then
        begin
            print_endline (Value.toStringVerbose card);
            display_front (Value.next card)
        end
    else
        print_endline (Value.toStringVerbose card)

let rec display_back card =
    print_endline (Value.toStringVerbose card);
    display_back (Value.previous card)

let () =
    print_endline "Print toInt T10:";
    print_int (Value.toInt Value.T10);
    print_char '\n';
    print_endline "\nPrint all value from T2 to As:";
    display_front Value.T2;
    print_char '\n';
    print_endline "Print all value from As to T2, then display exception:";
    display_back Value.As
